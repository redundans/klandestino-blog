<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Klandestino Blog
 */
?>

</div><!-- #page -->

<?php wp_footer(); ?>

<script type="text/javascript">
	jQuery(document).foundation('orbit', {
	  animation: 'fade',
	  animation_speed: 500,
	  stack_on_small: true,
	  navigation_arrows: true,
	  slide_number: false,
	  container_class: 'orbit-container',
	  stack_on_small_class: 'orbit-stack-on-small',
	  next_class: 'orbit-next',
	  prev_class: 'orbit-prev',
	  timer_container_class: 'orbit-timer',
	  timer_paused_class: 'paused',
	  timer_progress_class: 'orbit-progress',
	  slides_container_class: 'orbit-slides-container',
	  bullets_container_class: 'orbit-bullets',
	  bullets_active_class: 'active',
	  slide_number_class: 'orbit-slide-number',
	  caption_class: 'orbit-caption',
	  active_slide_class: 'active',
	  orbit_transition_class: 'orbit-transitioning',
	  bullets: true,
	  timer: false,
	  variable_height: false,
	  before_slide_change: function(){},
	  after_slide_change: function(){}
	});
</script>

</body>
</html>