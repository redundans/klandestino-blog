<?php
/**
 * Klandestino Blog functions and definitions
 *
 * @package Klandestino Blog
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

if ( ! function_exists( 'klandestino_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function klandestino_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on Klandestino Blog, use a find and replace
	 * to change 'klandestino' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'klandestino', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'klandestino' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

}
endif; // klandestino_setup
add_action( 'after_setup_theme', 'klandestino_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function klandestino_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'klandestino' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'klandestino_widgets_init' );

/**
 * Enqueue scripts and styles
 */
function klandestino_scripts() {
	wp_enqueue_script( 'jquery' );
	wp_register_script('foundation', get_template_directory_uri() . '/js/foundation/foundation.js', 'jquery');
	wp_register_script('foundation-orbit', get_template_directory_uri() . '/js/foundation/foundation.orbit.js', 'jquery');
	wp_enqueue_script( 'foundation' );
	wp_enqueue_script( 'foundation-orbit' );
	wp_enqueue_style( 'klandestino-style', get_stylesheet_directory_uri() . '/css/foundation.css' );
}
add_action( 'wp_enqueue_scripts', 'klandestino_scripts' );

remove_shortcode( 'gallery' );
function klandestino_gallary($attr) {
   
   	$output = '<div class="slideshow-wrapper"><div class="preloader"></div><ul class="orbit" data-orbit>';  	
	foreach ( explode(',',$attr[ids]) as $post_id ) {
		$image = get_post($post_id);
		$caption = $image->post_excerpt;
		$description = $image->post_content;
        if($description == '') $description = $title;
 		
 		$image_alt = get_post_meta($image->ID,'_wp_attachment_image_alt', true);
 
        $img = wp_get_attachment_image_src($image->ID, 'full');
        $output .= '<li><img src="'.$img[0].'"></li>';
	}
    $output .= '</ul></div>';
	return $output;
	}
add_shortcode( 'gallery' , 'klandestino_gallary' );

?>